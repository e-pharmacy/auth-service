/**
 * Copyright (C) 2020. Smart Bits GmbH. All rights reserved.
 * Created by Muhamed Vrajolli on 21/08/2020.
 */
package com.ubt.auth.authorization;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordEncoderConfig {
  @Bean
  public PasswordEncoder passwordEncoder() {
    return PasswordEncoderFactories.createDelegatingPasswordEncoder();
  }
}
