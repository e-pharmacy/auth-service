package com.ubt.auth.authorization;

import com.ubt.auth.commons.models.CustomUserDetailsModel;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.LinkedHashMap;
import java.util.Map;

public class CustomTokenEnhancer extends JwtAccessTokenConverter {
  @Override
  public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
    Map<String, Object> info = new LinkedHashMap<>(accessToken.getAdditionalInformation());

    if (authentication.getPrincipal() instanceof String) {
      DefaultOAuth2AccessToken customAccessToken = new DefaultOAuth2AccessToken(accessToken);
      customAccessToken.setAdditionalInformation(info);

      return super.enhance(customAccessToken, authentication);
    }

    CustomUserDetailsModel user = (CustomUserDetailsModel) authentication.getPrincipal();
    info.put("id", user.getId());
    info.put("location_id", user.getLocationId());
    info.put("first_name", user.getFirstName());
    info.put("last_name", user.getLastName());
    info.put("email", user.getUsername());
    info.put("registered_date", user.getCreatedDate());

    DefaultOAuth2AccessToken customAccessToken = new DefaultOAuth2AccessToken(accessToken);
    customAccessToken.setAdditionalInformation(info);

    return super.enhance(customAccessToken, authentication);
  }
}
