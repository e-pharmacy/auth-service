package com.ubt.auth.commons.jooqmappers;

import com.ubt.auth.commons.builders.CustomUserDetailsModelBuilder;
import com.ubt.auth.commons.date.DateTimeUtils;
import com.ubt.auth.commons.models.CustomUserDetailsModel;
import com.ubt.auth.commons.validation.ValidationService;
import com.ubt.auth.jooq.tables.records.UsersRecord;

import java.util.ArrayList;
import java.util.List;

public class CustomUserDetailsJooqMapper {
  public static CustomUserDetailsModel map(UsersRecord record) {
    CustomUserDetailsModel model = null;
    if (record != null) {
      model = CustomUserDetailsModelBuilder.aCustomUserDetailsModel()
              .withId(record.getId())
              .withLocationId(record.getLocationId())
              .withFirstName(record.getFirstName())
              .withLastName(record.getLastName())
              .withEmail(record.getEmail())
              .withPhone(record.getPhone())
              .withPassword(record.getPassword())
              .withEnabled(record.getEnabled())
              .withAccountExpired(record.getAccountExpired())
              .withCredentialsExpired(record.getCredentialsExpired())
              .withAccountLocked(record.getAccountLocked())
              .withCreatedDate(DateTimeUtils.toLong(record.getCreatedDate()))
              .withUpdatedDate(DateTimeUtils.toLong(record.getUpdatedDate()))
              .withCreatedBy(record.getCreatedBy())
              .withUpdatedBy(record.getUpdatedBy())
              .build();
    }
    return model;
  }

  public static UsersRecord unmap(CustomUserDetailsModel model) {
    UsersRecord record = new UsersRecord();

    if (model.getId() != null) {
      record.setId(model.getId());
    }
    if (model.getLocationId() != null) {
      record.setLocationId(model.getLocationId());
    }
    if (ValidationService.notBlank(model.getFirstName())) {
      record.setFirstName(model.getFirstName());
    }
    if (ValidationService.notBlank(model.getLastName())) {
      record.setLastName(model.getLastName());
    }
    if (ValidationService.notBlank(model.getUsername())) {
      record.setEmail(model.getUsername());
    }
    if (ValidationService.notBlank(model.getPhone())) {
      record.setPhone(model.getPhone());
    }
    if (ValidationService.notBlank(model.getPassword())) {
      record.setPassword(model.getPassword());
    }
    if (model.getCreatedBy() != null) {
      record.setCreatedBy(model.getCreatedBy());
    }
    if (model.getUpdatedBy() != null) {
      record.setUpdatedBy(model.getUpdatedBy());
    }

    return record;
  }

  public static List<CustomUserDetailsModel> map(List<UsersRecord> records) {
    List<CustomUserDetailsModel> models = new ArrayList<>();

    for (UsersRecord e : records) {
      models.add(map(e));
    }

    return models;
  }
}
