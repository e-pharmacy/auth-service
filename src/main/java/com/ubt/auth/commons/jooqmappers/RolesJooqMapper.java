package com.ubt.auth.commons.jooqmappers;

import com.ubt.auth.commons.builders.CustomGrantedAuthorityModelBuilder;
import com.ubt.auth.commons.models.CustomGrantedAuthorityModel;
import com.ubt.auth.commons.validation.ValidationService;
import com.ubt.auth.jooq.tables.records.RolesRecord;
import org.jooq.Record;

import java.util.ArrayList;
import java.util.List;

import static com.ubt.auth.jooq.Tables.ROLES;

public class RolesJooqMapper {
  public static CustomGrantedAuthorityModel map(Record record) {
    CustomGrantedAuthorityModel model = null;
    if (record != null) {
      model = CustomGrantedAuthorityModelBuilder.aCustomGrantedAuthorityModel()
              .withId(record.get(ROLES.ID))
              .withName(record.get(ROLES.NAME))
              .build();
    }
    return model;
  }

  public static RolesRecord unmap(CustomGrantedAuthorityModel model) {
    RolesRecord record = new RolesRecord();

    if (model.getId() != null) {
      record.setId(model.getId());
    }
    if (ValidationService.notBlank(model.getAuthority())) {
      record.setName(model.getAuthority());
    }

    return record;
  }

  public static List<CustomGrantedAuthorityModel> map(List<Record> records) {
    List<CustomGrantedAuthorityModel> models = new ArrayList<>();

    for (Record e : records) {
      models.add(map(e));
    }

    return models;
  }
}
