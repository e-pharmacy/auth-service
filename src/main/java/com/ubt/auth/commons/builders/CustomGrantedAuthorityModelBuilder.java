/**
 * Copyright (C) 2020. Smart Bits GmbH. All rights reserved.
 * Created by Muhamed Vrajolli on 21/08/2020.
 */
package com.ubt.auth.commons.builders;

import com.ubt.auth.commons.models.CustomGrantedAuthorityModel;

public final class CustomGrantedAuthorityModelBuilder {
  private Long id;
  private String name;

  private CustomGrantedAuthorityModelBuilder() {
  }

  public static CustomGrantedAuthorityModelBuilder aCustomGrantedAuthorityModel() {
    return new CustomGrantedAuthorityModelBuilder();
  }

  public CustomGrantedAuthorityModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public CustomGrantedAuthorityModelBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public CustomGrantedAuthorityModel build() {
    CustomGrantedAuthorityModel customGrantedAuthorityModel = new CustomGrantedAuthorityModel();
    customGrantedAuthorityModel.setId(id);
    customGrantedAuthorityModel.setName(name);
    return customGrantedAuthorityModel;
  }
}
