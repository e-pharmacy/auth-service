package com.ubt.auth.commons.builders;

import com.ubt.auth.commons.models.CustomGrantedAuthorityModel;
import com.ubt.auth.commons.models.CustomUserDetailsModel;

import java.util.List;

public final class CustomUserDetailsModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long createdBy;
  private Long updatedBy;
  private Long locationId;
  private String firstName;
  private String lastName;
  private String email;
  private String phone;
  private String password;
  private Boolean enabled;
  private Boolean accountLocked;
  private Boolean accountExpired;
  private Boolean credentialsExpired;
  private List<CustomGrantedAuthorityModel> authorities;

  private CustomUserDetailsModelBuilder() {
  }

  public static CustomUserDetailsModelBuilder aCustomUserDetailsModel() {
    return new CustomUserDetailsModelBuilder();
  }

  public CustomUserDetailsModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public CustomUserDetailsModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public CustomUserDetailsModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public CustomUserDetailsModelBuilder withCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public CustomUserDetailsModelBuilder withUpdatedBy(Long updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

  public CustomUserDetailsModelBuilder withLocationId(Long locationId) {
    this.locationId = locationId;
    return this;
  }

  public CustomUserDetailsModelBuilder withFirstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  public CustomUserDetailsModelBuilder withLastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  public CustomUserDetailsModelBuilder withEmail(String email) {
    this.email = email;
    return this;
  }

  public CustomUserDetailsModelBuilder withPhone(String phone) {
    this.phone = phone;
    return this;
  }

  public CustomUserDetailsModelBuilder withPassword(String password) {
    this.password = password;
    return this;
  }

  public CustomUserDetailsModelBuilder withEnabled(Boolean enabled) {
    this.enabled = enabled;
    return this;
  }

  public CustomUserDetailsModelBuilder withAccountLocked(Boolean accountLocked) {
    this.accountLocked = accountLocked;
    return this;
  }

  public CustomUserDetailsModelBuilder withAccountExpired(Boolean accountExpired) {
    this.accountExpired = accountExpired;
    return this;
  }

  public CustomUserDetailsModelBuilder withCredentialsExpired(Boolean credentialsExpired) {
    this.credentialsExpired = credentialsExpired;
    return this;
  }

  public CustomUserDetailsModelBuilder withAuthorities(List<CustomGrantedAuthorityModel> authorities) {
    this.authorities = authorities;
    return this;
  }

  public CustomUserDetailsModel build() {
    CustomUserDetailsModel customUserDetailsModel = new CustomUserDetailsModel();
    customUserDetailsModel.setId(id);
    customUserDetailsModel.setCreatedDate(createdDate);
    customUserDetailsModel.setUpdatedDate(updatedDate);
    customUserDetailsModel.setCreatedBy(createdBy);
    customUserDetailsModel.setUpdatedBy(updatedBy);
    customUserDetailsModel.setLocationId(locationId);
    customUserDetailsModel.setFirstName(firstName);
    customUserDetailsModel.setLastName(lastName);
    customUserDetailsModel.setEmail(email);
    customUserDetailsModel.setPhone(phone);
    customUserDetailsModel.setPassword(password);
    customUserDetailsModel.setEnabled(enabled);
    customUserDetailsModel.setAccountLocked(accountLocked);
    customUserDetailsModel.setAccountExpired(accountExpired);
    customUserDetailsModel.setCredentialsExpired(credentialsExpired);
    customUserDetailsModel.setAuthorities(authorities);
    return customUserDetailsModel;
  }
}
