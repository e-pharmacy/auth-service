package com.ubt.auth.commons.pageable;

import org.jooq.SortField;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Null safe org.springframework.data.domain.Pageable mapper model for database queries
 */
public class PageableModel {
  private Integer limit;
  private Long offset;
  private List<SortField<?>> orders;
  private Pageable pageable;

  protected PageableModel(Pageable pageable) {
    this.pageable = Pageable.unpaged();
    orders = new ArrayList<>();
    if (pageable != null && pageable.isPaged()) {
      limit = pageable.getPageSize();
      offset = pageable.getOffset();
      orders = PageableUtils.convertSortFields(pageable.getSort());
      this.pageable = pageable;
    }
  }

  public Integer getLimit() {
    return limit;
  }

  public Long getOffset() {
    return offset;
  }

  public List<SortField<?>> getOrders() {
    return orders;
  }

  public void addOrders(SortField<?>... orders) {
    if (this.orders == null) {
      this.orders = new ArrayList<>();
    }
    this.orders.addAll(Arrays.asList(orders));
  }

  public Pageable getPageable() {
    return pageable;
  }
}
