package com.ubt.auth.commons.validation;

import java.util.List;

public final class ValidationService {
  public static boolean notBlank(String input) {
    return input != null && !input.trim().isEmpty();
  }

  public static boolean notEmpty(List<?> input) {
    return input != null && !input.isEmpty();
  }

  public static boolean isEmpty(List<?> input) {
    return !notEmpty(input);
  }
}
