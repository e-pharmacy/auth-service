package com.ubt.auth.commons.models;

import org.springframework.security.core.GrantedAuthority;

public class CustomGrantedAuthorityModel implements GrantedAuthority {
  private Long id;
  private String name;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public String getAuthority() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
