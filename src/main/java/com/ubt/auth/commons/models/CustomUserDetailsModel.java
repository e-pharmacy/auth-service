package com.ubt.auth.commons.models;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CustomUserDetailsModel extends BoBaseModel implements UserDetails {
  private Long locationId;
  private String firstName;
  private String lastName;
  private String email;
  private String phone;
  private String password;
  private Boolean enabled;
  private Boolean accountLocked;
  private Boolean accountExpired;
  private Boolean credentialsExpired;
  private List<CustomGrantedAuthorityModel> authorities;

  public Long getLocationId() {
    return locationId;
  }

  public void setLocationId(Long locationId) {
    this.locationId = locationId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @Override
  public String getUsername() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Override
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public boolean isEnabled() {
    return enabled != null && enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  @Override
  public boolean isAccountNonExpired() {
    return accountExpired != null && !accountExpired;
  }

  public void setAccountExpired(Boolean accountExpired) {
    this.accountExpired = accountExpired;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return credentialsExpired != null && !credentialsExpired;
  }

  public void setCredentialsExpired(Boolean credentialsExpired) {
    this.credentialsExpired = credentialsExpired;
  }

  @Override
  public boolean isAccountNonLocked() {
    return accountLocked != null && !accountLocked;
  }

  public void setAccountLocked(Boolean accountLocked) {
    this.accountLocked = accountLocked;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();

    this.authorities.forEach(a -> authorities.add(new SimpleGrantedAuthority(a.getAuthority())));

    return authorities;
  }

  public void setAuthorities(List<CustomGrantedAuthorityModel> authorities) {
    this.authorities = authorities;
  }
}
