package com.ubt.auth.users;

import com.ubt.auth.commons.models.CustomUserDetailsModel;
import com.ubt.auth.users.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "userDetailsService")
public class UsersServiceImpl implements UsersService, UserDetailsService {
  private final UsersRepository usersRepository;

  @Autowired
  public UsersServiceImpl(UsersRepository usersRepository) {
    this.usersRepository = usersRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String input) {
    CustomUserDetailsModel user = usersRepository.findByUsername(input);

    if (user == null)
      throw new BadCredentialsException("Bad credentials");

    new AccountStatusUserDetailsChecker().check(user);

    return user;
  }

  @Override
  public Page<UserModel> getUsers(List<Long> usersIds, Pageable pageable) {
    return usersRepository.getUsers(usersIds, pageable);
  }

  @Override
  public UserModel getUserById(long userId) {
    return usersRepository.getUserById(userId);
  }

  @Override
  public UserModel createUser(UserModel userModel) {
    return usersRepository.createUser(userModel);
  }

  @Override
  public UserModel updateUser(long userId, UserModel userModel) {
    return usersRepository.updateUser(userId, userModel);
  }

  @Override
  public UserModel deleteUser(long userId) {
    return usersRepository.deleteUser(userId);
  }
}