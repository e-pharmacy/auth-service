package com.ubt.auth.users;

import com.ubt.auth.commons.jooqmappers.CustomUserDetailsJooqMapper;
import com.ubt.auth.commons.jooqmappers.RolesJooqMapper;
import com.ubt.auth.commons.models.CustomGrantedAuthorityModel;
import com.ubt.auth.commons.models.CustomUserDetailsModel;
import com.ubt.auth.commons.pageable.PageableModel;
import com.ubt.auth.commons.pageable.PageableUtils;
import com.ubt.auth.commons.validation.ValidationService;
import com.ubt.auth.jooq.tables.records.UsersRecord;
import com.ubt.auth.users.jooqmappers.UserJooqMapper;
import com.ubt.auth.users.models.UserModel;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.ubt.auth.jooq.Tables.*;

@Repository
public class UsersRepositoryImpl implements UsersRepository {
  private final DSLContext create;

  @Autowired
  public UsersRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public CustomUserDetailsModel findByUsername(String username) {
    CustomUserDetailsModel user = CustomUserDetailsJooqMapper.map(
            create.selectFrom(USERS)
                    .where(USERS.EMAIL.equalIgnoreCase(username))
                    .fetchOne()
    );

    if (user == null) {
      return null;
    }

    List<CustomGrantedAuthorityModel> authorities = RolesJooqMapper.map(
            create.select(ROLES.asterisk()).from(ROLES)
                    .join(USER_ROLES).onKey()
                    .where(USER_ROLES.USER_ID.eq(user.getId()))
                    .fetch()
    );

    user.setAuthorities(authorities);

    return user;
  }

  @Override
  public Page<UserModel> getUsers(List<Long> usersIds, Pageable pageable) {
    PageableModel customPageable = PageableUtils.createPageableModel(pageable);

    Condition condition = DSL.trueCondition();
    if (ValidationService.notEmpty(usersIds)) {
      condition = condition.and(USERS.ID.in(usersIds));
    }

    int totalElements = create.selectCount().from(USERS)
            .where(condition)
            .fetchOneInto(Integer.class);

    List<UserModel> userDetails = UserJooqMapper.map(
            create.selectFrom(USERS)
                    .where(condition)
                    .orderBy(customPageable.getOrders())
                    .offset(customPageable.getOffset())
                    .limit(customPageable.getLimit())
                    .fetch()
    );

    return PageableUtils.newPage(userDetails, customPageable, totalElements);
  }

  @Override
  public UserModel getUserById(long userId) {
    return UserJooqMapper.map(
            create.selectFrom(USERS)
                    .where(USERS.ID.eq(userId))
                    .fetchOne()
    );
  }

  @Override
  public UserModel createUser(UserModel userModel) {
    UsersRecord usersRecord = UserJooqMapper.unmap(userModel);

    return UserJooqMapper.map(
            create.insertInto(USERS)
                    .set(usersRecord)
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public UserModel updateUser(long userId, UserModel userModel) {
    UsersRecord usersRecord = UserJooqMapper.unmap(userModel);

    return UserJooqMapper.map(
            create.update(USERS)
                    .set(usersRecord)
                    .where(USERS.ID.eq(userId))
                    .returning()
                    .fetchOne()
    );
  }

  @Override
  public UserModel deleteUser(long userId) {
    return UserJooqMapper.map(
            create.delete(USERS)
                    .where(USERS.ID.eq(userId))
                    .returning()
                    .fetchOne()
    );
  }
}