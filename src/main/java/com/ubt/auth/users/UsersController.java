/**
 * Created by Muhamed Vrajolli on 21/08/2020.
 */
package com.ubt.auth.users;

import com.ubt.auth.users.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {
  private final UsersService usersService;

  @Autowired
  public UsersController(UsersService usersService) {
    this.usersService = usersService;
  }

  @GetMapping("")
  public Page<UserModel> getUsers(@RequestParam(required = false) List<Long> usersIds, Pageable pageable) {
    return usersService.getUsers(usersIds, pageable);
  }

  @GetMapping("/{userId}")
  @PreAuthorize("hasRole('admin')")
  public UserModel getUserById(@PathVariable long userId) {
    return usersService.getUserById(userId);
  }

  @PostMapping("")
  public UserModel createUser(@RequestBody UserModel userModel) {
    return usersService.createUser(userModel);
  }

  @PutMapping("/{userId}")
  public UserModel updateUser(@PathVariable long userId, @RequestBody UserModel userModel) {
    return usersService.updateUser(userId, userModel);
  }

  @DeleteMapping("/{userId}")
  public UserModel deleteUser(@PathVariable long userId) {
    return usersService.deleteUser(userId);
  }
}
