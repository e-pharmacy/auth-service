/**
 * Created by Muhamed Vrajolli on 21/08/2020.
 */
package com.ubt.auth.users;

import com.ubt.auth.users.models.UserModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UsersService {
  Page<UserModel> getUsers(List<Long> usersIds, Pageable pageable);

  UserModel getUserById(long userId);

  UserModel createUser(UserModel userModel);

  UserModel updateUser(long userId, UserModel userModel);

  UserModel deleteUser(long userId);
}
