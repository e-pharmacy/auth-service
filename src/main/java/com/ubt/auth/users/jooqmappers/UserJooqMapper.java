package com.ubt.auth.users.jooqmappers;

import com.ubt.auth.commons.date.DateTimeUtils;
import com.ubt.auth.commons.validation.ValidationService;
import com.ubt.auth.jooq.tables.records.UsersRecord;
import com.ubt.auth.users.builders.UserModelBuilder;
import com.ubt.auth.users.models.UserModel;

import java.util.ArrayList;
import java.util.List;

public class UserJooqMapper {
  public static UserModel map(UsersRecord record) {
    UserModel model = null;
    if (record != null) {
      model = UserModelBuilder.anUserModel()
              .withId(record.getId())
              .withLocationId(record.getLocationId())
              .withFirstName(record.getFirstName())
              .withLastName(record.getLastName())
              .withEmail(record.getEmail())
              .withPhone(record.getPhone())
              .withCreatedDate(DateTimeUtils.toLong(record.getCreatedDate()))
              .withUpdatedDate(DateTimeUtils.toLong(record.getUpdatedDate()))
              .withCreatedBy(record.getCreatedBy())
              .withUpdatedBy(record.getUpdatedBy())
              .build();
    }
    return model;
  }

  public static UsersRecord unmap(UserModel model) {
    UsersRecord record = new UsersRecord();

    if (model.getId() != null) {
      record.setId(model.getId());
    }
    if (model.getLocationId() != null) {
      record.setLocationId(model.getLocationId());
    }
    if (ValidationService.notBlank(model.getFirstName())) {
      record.setFirstName(model.getFirstName());
    }
    if (ValidationService.notBlank(model.getLastName())) {
      record.setLastName(model.getLastName());
    }
    if (ValidationService.notBlank(model.getPhone())) {
      record.setPhone(model.getPhone());
    }
    if (model.getCreatedBy() != null) {
      record.setCreatedBy(model.getCreatedBy());
    }
    if (model.getUpdatedBy() != null) {
      record.setUpdatedBy(model.getUpdatedBy());
    }

    return record;
  }

  public static List<UserModel> map(List<UsersRecord> records) {
    List<UserModel> models = new ArrayList<>();

    for (UsersRecord e : records) {
      models.add(map(e));
    }

    return models;
  }
}
