package com.ubt.auth.users.builders;

import com.ubt.auth.users.models.UserModel;

public final class UserModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long createdBy;
  private Long updatedBy;
  private Long locationId;
  private String firstName;
  private String lastName;
  private String email;
  private String phone;

  private UserModelBuilder() {
  }

  public static UserModelBuilder anUserModel() {
    return new UserModelBuilder();
  }

  public UserModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public UserModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public UserModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public UserModelBuilder withCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public UserModelBuilder withUpdatedBy(Long updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

  public UserModelBuilder withLocationId(Long locationId) {
    this.locationId = locationId;
    return this;
  }

  public UserModelBuilder withFirstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  public UserModelBuilder withLastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  public UserModelBuilder withEmail(String email) {
    this.email = email;
    return this;
  }

  public UserModelBuilder withPhone(String phone) {
    this.phone = phone;
    return this;
  }

  public UserModel build() {
    UserModel userModel = new UserModel();
    userModel.setId(id);
    userModel.setCreatedDate(createdDate);
    userModel.setUpdatedDate(updatedDate);
    userModel.setCreatedBy(createdBy);
    userModel.setUpdatedBy(updatedBy);
    userModel.setLocationId(locationId);
    userModel.setFirstName(firstName);
    userModel.setLastName(lastName);
    userModel.setEmail(email);
    userModel.setPhone(phone);
    return userModel;
  }
}
