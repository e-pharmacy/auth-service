INSERT INTO oauth_client_details (client_id,
                                  client_secret,
                                  resource_ids,
                                  scope,
                                  authorized_grant_types,
                                  web_server_redirect_uri,
                                  authorities,
                                  access_token_validity,
                                  refresh_token_validity,
                                  additional_information,
                                  autoapprove)
VALUES ('USER_CLIENT_APP',
        '{bcrypt}$2a$10$y4j2GTSDnVG57RsyguUe8.H0xoiyOxIz6HQpPwKc7scmNZozEzjGO',
        null,
        'read,write',
        'authorization_code,password,refresh_token,implicit',
        null,
        null,
        3600,
        10800,
        '{}',
        null);

INSERT INTO roles (id, name)
VALUES (1, 'admin'),
       (2, 'user');