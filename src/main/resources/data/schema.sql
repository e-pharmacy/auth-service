CREATE TABLE oauth_client_details
(
    client_id               VARCHAR(255) NOT NULL PRIMARY KEY,
    client_secret           VARCHAR(255) NOT NULL,
    resource_ids            VARCHAR(255),
    scope                   VARCHAR(255),
    authorized_grant_types  VARCHAR(255),
    web_server_redirect_uri VARCHAR(255),
    authorities             VARCHAR(10000),
    access_token_validity   INT,
    refresh_token_validity  INT,
    additional_information  VARCHAR(4096),
    autoapprove             VARCHAR(255)
);

CREATE TABLE roles
(
    id   BIGSERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);

CREATE TABLE users
(
    id                  BIGSERIAL PRIMARY KEY,
    first_name          VARCHAR(100) NOT NULL,
    last_name           VARCHAR(100) NOT NULL,
    email               VARCHAR(50)  NOT NULL UNIQUE,
    phone               VARCHAR(50),
    password            VARCHAR(100) NOT NULL,
    location_id         BIGINT NOT NULL,
    enabled             BOOLEAN      NOT NULL DEFAULT TRUE,
    account_expired     BOOLEAN      NOT NULL DEFAULT FALSE,
    credentials_expired BOOLEAN      NOT NULL DEFAULT FALSE,
    account_locked      BOOLEAN      NOT NULL DEFAULT FALSE,
    created_date        TIMESTAMP             DEFAULT CURRENT_TIMESTAMP,
    updated_date        TIMESTAMP,
    created_by          BIGINT       NOT NULL,
    updated_by          BIGINT
);

CREATE TABLE user_roles
(
    id      BIGSERIAL PRIMARY KEY,
    user_id BIGINT REFERENCES users (id) NOT NULL,
    role_id BIGINT REFERENCES roles (id) NOT NULL
);